var store = [{
        "title": "Snake for dummies (Flare-On 2019 challenge 8)",
        "excerpt":"Ever since I was 8 years old I thought snake was a game for dummies. To me it seemed more a test of patience than skill, a test I would always fail. But I knew if you were happy to take a scenic route, you could make a robot play...","categories": ["write-ups"],
        "tags": ["ctf","flare-on"],
        "url": "https://eric-horvat.github.io/Flare-On-Snake/",
        "teaser":null},{
        "title": "Sudoku Solver using SAT",
        "excerpt":"SAT solvers are great. You can use them to solve systems of equations, generate cd keys, or even go forwards or backwards in time in Conway’s Game of Life. When you use them you just list the constraints of a problem and then hit solve. In the past I’ve tried...","categories": ["write-ups"],
        "tags": ["maths"],
        "url": "https://eric-horvat.github.io/SAT-Solvers/",
        "teaser":null}]
