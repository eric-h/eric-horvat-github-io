# Snake for dummies
Ever since I was 8 years old I thought snake was a game for dummies. To me it seemed more a test of patience than skill, a test I would always fail. But I knew if you were happy to take a scenic route, you could make a robot play the game and you wouldn’t even have to be that smart.

A month ago I was participating in Flare-On, a CTF made for reverse engineers, by cyber security company FireEye. They base their challenges on real-world malware they’ve had to analyse. They like to include a “for fun” challenge and this year’s was to beat a snake game built for the NES. 

< insert press start >

I guess the intended solution for the challenge was to cheat. Learn how the game is coded so you can find a vulnerability and determine the flag. However, I saw an opportunity to make my dream come true. FCEUX is a NES emulator which offers Lua (similar to python) scripting. With Lua I was able to write to arbitrary addresses in memory. And that’s all I needed. The game was simple enough that I only ever read from one or two bytes in the entirety of memory and wrote ever wrote to one byte.

At the bottom of this post I’ve attached the code I used below, it’s simple enough for anybody to understand. I even activated “turbo mode” to make it run super fast. 



![](Snake%20for%20dummies/trimmed-slow-af.gif)
![](Snake%20for%20dummies/fast-and-trimmed.gif)

If I’d bothered to actually reverse engineer the game I’d have learned I needed to eat 25 apples, beating the game 4 times in a row to meet the victory condition. Instead, it looked to me that after a few minutes the snake would crash into itself. Thankfully I left it running while I went outside, confused, and I came back to a victory flag.
	
< insert conclusion >

< code >